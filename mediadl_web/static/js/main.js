
function make_video_info_dom(data) {
    item_id = data["id"]
    return $("<div>", {
        css: {
            "display": "flex",
            "margin-bottom": "0.5em",
            "margin-top": "0.5em",
            "height": "7em"
        }
    }).append(
        $("<div>").append(  
            $("<img>", {
                src: data["thumbnail"],
                css: {
                    "object-fit": "cover",
                    "height": "6em",
                    "width": "9.6em"
                },
                onerror: (
                    "this.onerror=null; this.src='" + 
                    window.MediaDL.base_path("/static/missing_video.svg") +
                    "'"
                )
            }),
            $("<div>").addClass("progress").append(
                $("<div>", {
                    id: 'dl_progress_' + item_id,
                })
            )
        ),
        $("<div>", {
            css: {
                "padding-left": "1.5em",
                "padding-right": "0.5em",
                "text-align": "left",
                "margin-right": "0.5em"
            }
        }).append(
            $("<span>").append(
                $("<a>", {
                    href: data["url"],
                    css: {
                        "font-weight": "bold",
                        "font-size": "1.3em"
                    }
                }).text(data["title"])
            ),
            $("<p>", {
                css: {
                    "font-size": "0.9em"
                }
            }).text(data["uploader"])
        ),
        $("<span>", {
            "css": {
                "line-height": "6em",
                "height": "6em",
                "margin-left": "auto"
            }
        }).append(
            $("<a>", {
                id: 'dl_link_' + item_id,
                css: {
                    "font-size": "2em"
                }
            }).append(
                $("<i>", {
                    id: 'dl_link_symbol_' + item_id,
                })
            )
        )
    );
}

function queue_video(endpoint, url, playlist_index) {
    $.ajax({
        url: endpoint,
        data: {
            "url": encodeURIComponent(url),
            "playlist_index": parseInt(playlist_index)
        },
        type: 'get'
    }).fail(function() {
        console.log("Failed to queue video.")
    });
}

function update_item_status(endpoint, endpoint_queue, endpoint_download, ids) {
    if(ids.length == 0)
        return;

    $.ajax({
        url: endpoint,
        data: {
            "ids": Object.keys(ids).join()
        },
        type: 'get'
    }).done(function(responseData) {
        for(const [item_id, url] of Object.entries(ids))  {
            var dl_progress = $(document.getElementById("dl_progress_" + item_id));
            var dl_link_symbol = $(document.getElementById("dl_link_symbol_" + item_id));
            var dl_link = $(document.getElementById("dl_link_" + item_id));

            if(responseData[item_id]["status"] == "new") {
                if(dl_progress.hasClass("_state_new"))
                    continue;

                dl_link_symbol.removeClass();
                dl_link_symbol.addClass("fas fa-cloud-download-alt");

                dl_link.click(function (e) {
                    var playlist_index = item_id.split(":")[1];
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    queue_video(endpoint_queue, url, playlist_index);
                    return false;
                });

                dl_progress.removeClass()
                dl_progress.addClass("progress-empty _state_new")
                dl_progress.css({
                    width: "100%"
                });
            } else if(responseData[item_id]["status"] == "downloading") {
                if(dl_progress.hasClass("_state_downloading")) {
                    if("progress" in responseData[item_id]) {
                        dl_progress.removeClass()
                        dl_progress.addClass("progress-full _state_downloading")
                        dl_progress.css({
                            width: responseData[item_id]["progress"]
                        });
                    }
                    continue;
                }

                dl_link_symbol.removeClass()
                dl_link_symbol.addClass("fas fa-circle-notch fa-spin")

                dl_link.off('click');

                if("progress" in responseData[item_id]) {
                    dl_progress.removeClass()
                    dl_progress.addClass("progress-full _state_downloading")
                    dl_progress.css({
                        width: responseData[item_id]["progress"]
                    });
                } else {
                    dl_progress.removeClass()
                    dl_progress.addClass("progress-content-unknown _state_downloading")
                    dl_progress.css({
                        width: "19.2em"
                    });
                }
            } else if(responseData[item_id]["status"] == "queued") {
                if(dl_progress.hasClass("_state_queued"))
                    continue;

                dl_link_symbol.removeClass()
                dl_link_symbol.addClass("fas fa-circle-notch fa-spin")

                dl_link.off('click');

                dl_progress.removeClass()
                dl_progress.addClass("progress-empty _state_downloading")
                dl_progress.css({
                    width: "19.2em"
                });
            } else if(responseData[item_id]["status"] == "downloaded") {
                if(dl_progress.hasClass("_state_downloaded"))
                    continue;

                dl_link_symbol.removeClass()
                dl_link_symbol.addClass("fas fa-file-download")

                dl_link.attr({
                    "href": endpoint_download + "/" + encodeURIComponent(item_id),
                    "target": "_blank"
                })

                dl_progress.removeClass()
                dl_progress.addClass("progress-full _state_downloaded")
                dl_progress.css({
                    width: "100%"
                });
            }
        }
    }).fail(function() {
        console.log("Failed to retrieve item status")
    });
}

function build_video_list(endpoint, endpoint_queue, endpoint_download, arr) {
    info_box = $("#videos-info")
    
    info_box.empty();

    if(arr.length == 0) {
        info_box.css("display", "none");
        return;
    }

    var ids = {};

    arr.forEach(function (e) {
        if(e["id"] != "") {
            info_box.append(
                make_video_info_dom(e)
            );
            ids[e["id"]] = e["url"];
        }
    });
    update_item_status(endpoint, endpoint_queue, endpoint_download, ids)

    if(Object.keys(ids).length > 0) {
        window.item_status_timer = setInterval(
            update_item_status,
            1000,
            endpoint,
            endpoint_queue,
            endpoint_download,
            ids
        );
    }
}

function submit_url(endpoint, endpoint_timer, endpoint_queue, endpoint_download, value) {
    var status_indicator = $("#status-indicator");
    var info_box = $("#videos-info");
    var go_button = $("#go-button");
    var input_field = $('#url-input')

    go_button.prop('disabled', true);
    input_field.prop('disabled', true);

    status_indicator.removeClass();
    status_indicator.addClass("fas fa-sync fa-spin");
    status_indicator.css("color", "#daa520");

    clearInterval(window.item_status_timer);

    $.ajax({
        url: endpoint,
        data: {
            "value": value,
            "only_validate": false
        },
        type: 'get'
    }).done(function(responseData) {
        status_indicator.removeClass();
        status_indicator.addClass("fas fa-check-double");
        status_indicator.css("color", "#22b222");

        info_box.css("display", "flex");
        
        go_button.prop('disabled', false);
        input_field.prop('disabled', false);

        build_video_list(endpoint_timer, endpoint_queue, endpoint_download, responseData["entries"]);
    }).fail(function() {
        status_indicator.removeClass();
        status_indicator.addClass("fas fa-times");
        status_indicator.css("color", "#b22222");

        info_box.css("display", "none");

        go_button.prop('disabled', false);
        input_field.prop('disabled', false);
    });
}

function check_url(endpoint, value){
    var status_indicator = $("#status-indicator");
    var info_box = $("#videos-info");
    var go_button = $("#go-button");

    if(value == this.previous_value)
        return;
    
    this.previous_value = value;
    info_box.css("display", "none");

    $.ajax({
        url: endpoint,
        data: {
            "value": value,
            "only_validate": true
        },
        type: 'get'
    }).done(function(responseData) {
        status_indicator.removeClass();
        status_indicator.addClass("fas fa-check");
        status_indicator.css("color", "#22b222");
        
        go_button.prop('disabled', false);
    }).fail(function() {
        status_indicator.removeClass();
        status_indicator.addClass("fas fa-times");
        status_indicator.css("color", "#b22222");

        go_button.prop('disabled', true);
    });
}
