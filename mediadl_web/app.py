import re
import hashlib
from pathlib import Path
from queue import Queue
import threading
import urllib.parse
import time
import os


from flask import Flask
from flask import render_template
from flask import request
from flask import send_from_directory

from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

import youtube_dl

import json

app = Flask(__name__, template_folder='templates', static_folder='static')

BASE_PATH = os.environ.get("MEDIADL_BASE_PATH", "")
TARGET_DIRECTORY = os.environ.get("MEDIADL_DOWNLOAD_FOLDER", "../dl_folder")
MAX_QUEUE = 100

def url_id(base_url, playlist_index):
    if not base_url:
        return ""

    m = hashlib.md5()

    m.update(urllib.parse.unquote(base_url).encode())
    return "V" + m.hexdigest() + f":{playlist_index}"

def id_to_filename(item_id):
    item_hash, playlist_idx = item_id.split(":")

    assert(item_hash.isalnum())
    playlist_idx = int(playlist_idx)

    return f"{item_hash}_{playlist_idx}"

class DownloadHandler:
    def __init__(self, target_directory, max_queue):
        self.current_download = {
            "id": None,
            "url": None,
            "progress": None
        }
        self.download_queue = {}
        
        self.mutex = threading.RLock()
        self.dl_event = threading.Event()

        self.thread = threading.Thread(
            target=self._download_watcher
        )
        self.thread.start()

        self.target_directory = target_directory
        self.max_queue = max_queue

    def queue_new(self, url, playlist_index):
        if len(self.download_queue) >= self.max_queue:
            raise RuntimeError("Queue is already full.")

        with self.mutex:
            self.download_queue[url_id(url, playlist_index)] = url

        self.dl_event.set()

    def _progress_hook(self, status):
        if "total_bytes" in status and "downloaded_bytes" in status:
            self.current_download["progress"] = (
                100 * status["downloaded_bytes"] / status["total_bytes"]
            )
        else:
            self.current_download["progress"] = None

    def _download_watcher(self):
        while True:
            self.dl_event.wait()

            with self.mutex:
                if self.download_queue:
                    item_id = next(iter(self.download_queue.keys()))

                    self.current_download["id"] = item_id
                    self.current_download["url"] = self.download_queue[item_id]
                    self.current_download["progress"] = None

                    del self.download_queue[item_id]
                else:
                    self.dl_event.clear()

            if self.current_download["id"] is not None:
                item_id = self.current_download["id"]
                item_url = urllib.parse.unquote(self.current_download["url"])

                item_hash, playlist_idx = item_id.split(":")

                try:
                    playlist_idx = int(playlist_idx)

                    filename = id_to_filename(item_id)
                    ytdl_conf = {
                        "outtmpl": f"{self.target_directory}/active_{item_hash}_%(playlist_index)s",
                        "progress_hooks": [self._progress_hook]
                    }
                    if playlist_idx >= 0:
                        ytdl_conf["playlist_items"] = f"{playlist_idx}"

                    with youtube_dl.YoutubeDL(ytdl_conf) as ydl:
                        pass
                        ydl.cache.remove()
                        info_dict = ydl.extract_info(item_url, download=False)
                        ydl.prepare_filename(info_dict)
                        ydl.download([item_url])

                        for p in Path(self.target_directory).glob("active_*"):
                            _, item_hash, playlist_idx = p.stem.split("_")
                            if playlist_idx == "NA":
                                playlist_idx = 0
                            else:
                                playlist_idx = int(playlist_idx)
                            
                            p.rename(p.parent / (f"{item_hash}_{playlist_idx}" + p.suffix))

                except ValueError:
                    print("Failed to parse playlist index: skipping queue entry...")
                finally:
                    self.current_download["id"] = None
                    self.current_download["url"] = None
                    self.current_download["progress"] = None


    def get_download_states(self, ids):
        result = {}
        for item_id in ids:
            result[item_id] = {}

            filename = id_to_filename(item_id)

            if item_id == self.current_download["id"]:
                result[item_id]["status"] = "downloading"
                if self.current_download["progress"] is not None:
                    result[item_id]["progress"] = f'{self.current_download["progress"]}%'
            elif list(Path(self.target_directory).glob(f"{filename}*")):
                result[item_id]["status"] = "downloaded"
            else:
                with self.mutex:
                    if item_id in self.download_queue.keys():
                        result[item_id]["status"] = "queued"
                    else:
                        result[item_id]["status"] = "new"
        return result

download_hander = DownloadHandler(TARGET_DIRECTORY, MAX_QUEUE)

def compose_info_response(base_url, result):
    thumbail = result.get("thumbnails", None)
    if thumbail:
        thumbail = thumbail[0]["url"]
    else:
        thumbail = ""

    result["playlist_index"] = result.get("playlist_index", 0)
    if result["playlist_index"] is None:
        result["playlist_index"] = 0

    return {
        "id": url_id(base_url, result["playlist_index"]),
        "title": result.get("title", ""),
        "uploader": result.get("uploader", ""),
        "thumbnail": thumbail,
        "url": base_url
    }

@app.route(f'{BASE_PATH}/static/<path:path>')
def serve_static(path):
    return send_from_directory('static', path)

@app.route(f'{BASE_PATH}/js/<path:path>')
def serve_js(path):
    return send_from_directory('static/js', path)

@app.route(f'{BASE_PATH}/css/<path:path>')
def serve_css(path):
    return send_from_directory('static/css', path)

@app.route(f'{BASE_PATH}/download/<string:item_id>')
def download(item_id):
    filename = id_to_filename(item_id)

    files = [p.name
        for p in Path(TARGET_DIRECTORY).glob(f"{filename}*")
    ]
    return send_from_directory(
        TARGET_DIRECTORY,
        files[0],
        as_attachment=True,
        attachment_filename=files[0]
    )

@app.route(f'{BASE_PATH}/')
def root():
    return render_template("index.html",
        base_path=BASE_PATH
    )

@app.route('/favicon.ico')
def favicon():
    return app.send_static_file("favicon.png")

@app.route(f'{BASE_PATH}/download-states', methods=['GET'])
def download_states():
    ids = request.args.get('ids')
    m = re.match(r'[a-zA-Z0-9]+\:\d+(,[a-zA-Z0-9]+:\d+)*', ids)
    if not m:
        return "", 400
    
    ids = ids.split(",")

    return download_hander.get_download_states(ids)

@app.route(f'{BASE_PATH}/queue', methods=['GET'])
def queue_url():
    url = request.args.get('url')
    playlist_index = request.args.get('playlist_index')
    download_hander.queue_new(url, playlist_index)

    return "OK", 200

@app.route(f'{BASE_PATH}/url-info', methods=['GET'])
def url_info():
    val = URLValidator()
    url = request.args.get('value')
    only_validate = request.args.get('only_validate')

    m = re.search(r'^[a-zA-Z0-9\.\-]+\:\/\/', url)
    if not m:
        url = f"https://{url}"

    try:
        val(url)
    except ValidationError:
        return "", 400

    if only_validate == "true":
        return {"entries": []}

    with youtube_dl.YoutubeDL() as ydl:
        try:
            result = ydl.extract_info(url, download=False)
        except youtube_dl.utils.DownloadError:
            return "", 400

    if "_type" in result:
        return {"entries": [compose_info_response(url, e) for e in result['entries']]}
    
    return {"entries": [compose_info_response(url, result)]}

if __name__ == '__main__':
    app.run()
