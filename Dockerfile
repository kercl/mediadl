FROM python:3.7-slim-buster

ADD requirements.txt /mediadl/requirements.txt
RUN apt-get update && \
    apt-get install -y ffmpeg && \
    pip install -r /mediadl/requirements.txt

ADD . /mediadl
WORKDIR /mediadl/mediadl_web

EXPOSE 5000

ENV MEDIADL_DOWNLOAD_FOLDER=/data
CMD ["flask", "run", "--host", "0.0.0.0"]
